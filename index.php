<?php
  session_start();

  require_once("scoop_it/ScoopIt.php");
  require_once("scoop_it/config.php");
  
  // Construct scoop var, which handle API communication
  $scoop = new ScoopIt(new SessionTokenStore(), $localUrl, $consumerKey, $consumerSecret);
   
  // Login in, if not previously logged in, it will issue a redirection
  // to scoop.it servers to log the user in. 
  // You can omit the call below if you want to use the api in "anonymous"
  // mode.
  $scoop->login();
  $currentUser = $scoop->profile(null)->user;
  
  $business = $scoop->topic(699510)->curatedPosts;
  $cloud = $scoop->topic(701004)->curatedPosts;
  $mobile = $scoop->topic(906627)->curatedPosts;

  //$scoop->thankAPost(3190067741);
  $scoop->commentAPost(3190067741,"test comment");
  ?>  
<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>scoop it page</title>
  
  <!-- Included CSS Files (Uncompressed) -->
  <!--
  <link rel="stylesheet" href="stylesheets/foundation.css">
  -->
  
  <!-- Included CSS Files (Compressed) -->
  <link rel="stylesheet" href="stylesheets/foundation.css">
  <link rel="stylesheet" href="stylesheets/scoop.css">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <script src="javascripts/modernizr.foundation.js"></script>
  <script>
    $(document).ready(function(){
      $(".rescoop").click(function(){
        var pid = $(this).data("post");
        $("#rescoopFrame").attr("src","http://www.scoop.it/bookmarklet?postLidToRescoop="+pid);
        $("#myModal").reveal();
        return false;
      });
    });
  </script>
  <!-- IE Fix for HTML5 Tags -->
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

</head>
<body>
<div id="myModal" class="reveal-modal [expand, xlarge, large, medium, small]">
  <iframe id="rescoopFrame" src="" width="420" height="715"></iframe>
  <a class="close-reveal-modal">&#215;</a>
</div>
<div class="row scoop">
  <div class="scooptop">
      <a class="button" href="">Follow</a>
  </div>  
  <div class="row scooptopics">
    <section class="scoopbusiness four columns">
      <h2>Business</h2>
      <hr>
        <?php
          for($index=0;$index<count($business);$index++){

            $id      = $business[$index]->id;

            $title   = $business[$index]->title;
            $content = $business[$index]->content;
            if(isset($business[$index]->imageUrl) or !empty($business[$index]->imageUrl)){
              $img     = $business[$index]->imageUrl;
              $imgPos  = $business[$index]->imagePosition;
            }
            $url     = $business[$index]->url;
            $from    = parse_url($url,PHP_URL_HOST);
            $dateCur = substr($business[$index]->curationDate,0,-3);
            $dt      = date('r', $dateCur);
            $newDt   = new DateTime($dt);
            $date    = $newDt->format("F, d Y");

             //$scoop->rescoop($business[$index]->id,$business[$index]->topicId);


         ?>
             <section class="scoopbox">
              <p><img src="images/rc.jpg" alt=""> <span>Scooped by <a href="">RingCentral</a></span></p>
              <hr>
              <section class="row">
                <section class="eight column">
                  <h2><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h2>
                </section>
                <section class="four column">
                  <a class="rescoop" href="" data-post="<?php echo $id; ?>">rescoop</a>
                </section>
              </section>
                
                <article class="row">
                   <?php
                    if(isset($business[$index]->imageUrl) or !empty($business[$index]->imageUrl)){
                    ?><img src="<?php echo $img; ?>" alt="<?php echo $title; ?>"><?php
                    }
                    ?>
                    <section>
                      <p>
                        <b>From <?php echo $from; ?></b> <br>
                        <?php echo $date; ?>
                      </p>
                      <p>
                        <?php echo $content; ?>
                      </p>
                    </section>
              </article>
            </section>
            <!-- eof scoopbox -->
            <?php
          }
        ?>
       
    </section>

    <section class="scoopcloud four columns">
      <h2>Cloud</h2>
      <hr>

     <?php
          for($index=0;$index<count($cloud);$index++){
            $title   = $cloud[$index]->title;
            $content = $cloud[$index]->content;
            if(isset($cloud[$index]->imageUrl) or !empty($cloud[$index]->imageUrl)){
              $img     = $cloud[$index]->imageUrl;
              $imgPos  = $cloud[$index]->imagePosition;
            }
            $url     = $cloud[$index]->url;
            $from    = parse_url($url,PHP_URL_HOST);
            $dateCur = substr($cloud[$index]->curationDate,0,-3);
            $dt      = date('r', $dateCur);
            $newDt   = new DateTime($dt);
            $date    = $newDt->format("F, d Y");

         ?>
             <section class="scoopbox">
              <p><img src="images/rc.jpg" alt=""> <span>Scooped by <a href="">RingCentral</a></span></p>
              <hr>
              <section class="row">
                <section class="eight column">
                  <h2><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h2>
                </section>
                <section class="four column">
                  <a class="rescoop" href="">rescoop</a>
                </section>
              </section>
                
                <article class="row">
                   <?php
                    if(isset($business[$index]->imageUrl) or !empty($business[$index]->imageUrl)){
                    ?><img src="<?php echo $img; ?>" alt="<?php echo $title; ?>"><?php
                    }
                    ?>
                    <section>
                      <p>
                        <b>From <?php echo $from; ?></b> <br>
                        <?php echo $date; ?>
                      </p>
                      <p>
                        <?php echo $content; ?>
                      </p>
                    </section>
              </article>
            </section>
            <!-- eof scoopbox -->
            <?php
          }
        ?>
    </section>

    <section class="scoopmobile four columns">
      <h2>Mobile</h2>
      <hr>
<?php
          for($index=0;$index<count($mobile);$index++){
            $title   = $mobile[$index]->title;
            $content = $mobile[$index]->content;
            if(isset($mobile[$index]->imageUrl) or !empty($mobile[$index]->imageUrl)){
              $img     = $mobile[$index]->imageUrl;
              $imgPos  = $mobile[$index]->imagePosition;
            }
            $url     = $mobile[$index]->url;
            $from    = parse_url($url,PHP_URL_HOST);
            $dateCur = substr($mobile[$index]->curationDate,0,-3);
            $dt      = date('r', $dateCur);
            $newDt   = new DateTime($dt);
            $date    = $newDt->format("F, d Y");

         ?>
             <section class="scoopbox">
              <p><img src="images/rc.jpg" alt=""> <span>Scooped by <a href="">RingCentral</a></span></p>
              <hr>
              <section class="row">
                <section class="eight column">
                  <h2><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h2>
                </section>
                <section class="four column">
                  <a class="rescoop" href="">rescoop</a>
                </section>
              </section>
                
                <article class="row">
                  <?php
                  if(isset($business[$index]->imageUrl) or !empty($business[$index]->imageUrl)){
                  ?><img src="<?php echo $img; ?>" alt="<?php echo $title; ?>"><?php
                  }
                  ?>
                    <section>
                      <p>
                        <b>From <?php echo $from; ?></b> <br>
                        <?php echo $date; ?>
                      </p>
                      <p>
                        <?php echo $content; ?>
                      </p>
                    </section>
              </article>
            </section>
            <!-- eof scoopbox -->
            <?php
          }
        ?>
    </section>
  </div>
</div>
 
  

  <!-- Included JS Files (Compressed) -->
  <script src="javascripts/jquery.js"></script>
  <script src="javascripts/foundation.min.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="javascripts/app.js"></script>

  
    <script>
    $(window).load(function(){
      $("#featured").orbit();
    });
    </script> 
  
</body>
</html>